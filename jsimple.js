/**
* jSimple
* Desc: Table Builder Plug in for jQuery Library
* Author: (c)2012 william Rivera
* Original Date: 10/1/2011
* Revision Date: 2/25/2017
* Verision: 2.0
* May be freely distributed according to MIT license.
*/
(function (J$) {
    J$.fn.Simplify = function (Data) {
        function table(defaults,elem, Data) {
            
            var selected = J$.extend({}, defaults, Data);
            var data = Data.data.slice(0);
			var that = this;
			
            this.header = data.splice(0, 1);
            this.data = data;
            this.index = 0;
            this.limit = selected.limit;
            this.currentPage = 1;
            this.tableClassName = selected.tableClassName;
			this.pager = selected.pager;
			this.paginateFn=function(){return selected.pager.pageControl.call(this);};
			this.paginate=selected.paginate;
			
            sort.sortable = selected.makeSortable;

            var table = J$('<div></div>').attr({ 'class': selected.divClass });
            this.makeTable = function () {
                var data = simplify.generate.call(this);
                table.append(data);
				return this;
            }
            this.clear = function () {
                table.find("table").remove();
                return this;
            }
            this.addRow = function (row) {
                data = simplify.BuildTableData(row);
                var headerData = data.splice(0, 1);
                if (this.header[0].length == 0)
                    this.header = headerData;
                if (this.data == undefined)
                    this.data = data;
                else
                    this.data.push(data[0]);
                this.clear();
                this.makeTable();
            }
            this.makeTable();
			this.table=table;
			this.elem=elem;
			this.paginateFn();
        }
        function link(table, num) {
            this.index = num;
            this.elem = J$('<span></span>').html(num);
            var l = this;
            if (table.currentPage != (this.index)) {
                this.elem.attr({ 'class': 'simple_paginate' });
                this.elem.click(function () {
					table.pager.updatePage(table,l);
                });
            } else {
                this.elem.attr({ 'class': 'simple_paginate simple_paginate_active' });
            }
        }
		var pager={
			pageControl:function(){
				var table =this.table;
				var elem = this.elem;
				table.append(simplify.paginate.call(this));
				J$(elem).append(table);
				return this;
			},
			getN:function(){
				return this.index;
			},
			len:function(){
                return Math.min((this.limit + this.index), this.data.length);
			},
			index:function(){
				return (this.index + 1);
			},
			total:function(){
				return this.data.length;
			},
			sort:function(){
				this.data = sort.sortData(this.data);
				this.clear().makeTable().paginateFn();
			},
			updatePage:function(table,l){
				table.index = (l.index - 1) * table.limit;
                table.currentPage = l.index;
                table.clear().makeTable().paginateFn();
			}
		};
		var sort = {
            sortable: true,
            sortOn: "none",
            sortBy: "Asc",
            sortData: function (data) {
                var arr = [];
                for (var j = 0, ll = data.length; j < ll; j++) {
                    var col = {};
                    col.row = data[j];
                    col.val = data[j][this.sortOn].sort || data[j][this.sortOn].data;
                    col.index = (isNaN(parseInt(col.val))) == true || typeof(col.val)=="number"? col.val : col.val.toLowerCase();
                    arr.push(col);
                }

                if (this.sortBy == "Asc")
                    arr.sort(this.ascending);
                else
                    arr.sort(this.descending);

                var d = [];
                for (j = 0; j < arr.length; j++)
                    d.push(arr[j].row);
                return d;
            },
            ascending: function (a, b) {
                if (a.index == b.index) return 0;
                return a.index > b.index ? 1 : -1;
            },
            descending: function (a, b) {
                if (a.index == b.index) return 0;
                return a.index < b.index ? 1 : -1;
            }
        }
        var simplify = {
            generate: function () {
                var that = this;
                var selectedData = this.header.slice(0, 1);
                if (!this.paginate) {
                    var len = this.data.length;
                    var n = 0;
                } else {
					n=this.pager.getN.call(this);
					len=this.pager.len.call(this);
                }
				
                for (n; n < len; n++)
                    selectedData.push(this.data[n]);

                var tbl = J$("<table></table>").attr({ 'class': this.tableClassName });
                var datum = selectedData.slice();
                selectedData.each = each;
                selectedData.each(
                function (index) {//each row 
                    var cls = (index % 2 == 0) ? "even" : "odd";
                    var trow = J$("<tr></tr>").attr({ 'class': cls });
                    tbl.append(trow);
                    this.each = each;
                    this.each(
                            function (i) {
                                var tcell = J$('<td></td>');
                                tcell.html(this.data);
                                if (index == 0 && (Number(i) || i == 0) && sort.sortable && this.sort != "none") {
                                    var img = J$(document.createElement("img"));
                                    img.attr({ "class": "sort_arrow" });

                                    if (sort.sortOn == i) {
                                        if (sort.sortBy == "Asc")
                                            img.attr({ "src": "data:image/jpeg;base64," + Images.up() });
                                        else
                                            img.attr({ "src": "data:image/jpeg;base64," + Images.down() });
                                    } else {
                                        img.attr({ "src": "data:image/jpeg;base64," + Images.arrows() });
                                    }
                                    tcell.append(img);
                                    tcell.click(function () {
                                        sort.sortOn = i;
                                        if (sort.sortBy == "Asc")
                                            sort.sortBy = "Desc";
                                        else
                                            sort.sortBy = "Asc";
										that.pager.sort.call(that);
                                    });
                                }
                                if (this.sort)
                                    tcell.attr({ "sort": this.sort });
                                if (this['class'])
                                    tcell.attr({ "class": this['class'] });
                                trow.append(tcell);
                            }
                    );
                });
                return tbl;
            },
            paginate: function () {
                var tbl = J$("<table></table>").attr({ 'class': 'simple_tablebot' });
                var trow = J$("<tr></tr>");

                var count = this.pager.len.call(this);
                var total = this.pager.index.call(this);;
                if (this.pager.total.call(this) == 0)
                    total = 0;
                if (count != total) total += "-" + count;
                total += " of " + this.pager.total.call(this);

                var res = J$("<td></td>").html("Showing: " + total);
                var rightCol = J$("<td></td>").attr({ "align": "right" });
                var paginate = J$("<div></div>").attr({ 'class': 'simple_paginate_section' });

                var pagesCount = Math.ceil(this.pager.total.call(this) / this.limit);
                var table = this;
                if (pagesCount > 1) {
                    for (var i = 1; i <= pagesCount; i++) {
                        var l = new link(table, i);
                        paginate.append(l.elem);
                    }
                }
                rightCol.append(paginate)
                trow.append(res).append(rightCol);
                tbl.append(trow);
                return tbl;
            },
            BuildTableData: function (data) {
                var header = GetHeaders(data[0]);
                var tabledata = [];
                var headerdata = [];

                for (var i = 0; i < header.length; i++)
                    headerdata.push(parseSort(header[i]));

                tabledata.push(headerdata);

                for (var i = 0; i < data.length; i++) {
                    var p = data[i];
                    var item = [];
                    for (var key in p) {
                        if (typeof (p[key]) != 'function')
                            item.push(parseSort(p[key]));
                    }
                    tabledata.push(item);
                }
                function parseSort(val) {
                    var regx = /\[[\,\d\w\s\(\)\/\'\;(class\:|sort\:)\.\-\_]{1,}\]/;
                    val += "";
                    var t = val.match(regx);
                    if (!t) return { "data": val };
                    var pair = val.split(regx);

                    var match = val.substring(val.search(regx) + 1, val.search(/\]/));
                    var opts = match.split(";");
                    var options = { "data": pair.join("") };
                    for (var i = 0; i < opts.length; i++) {
                        var params = opts[i].split(":");
                        options[params[0]] = params[1];
                    }
                    return options;
                }
                function GetHeaders(obj) {
                    var cols = [];
                    var p = obj; for (var key in p) {
                        if (typeof (p[key]) != 'function')
                            cols.push(key);
                    }
                    return cols;
                }
                return tabledata;
            }
        }
        var each = function (func) {
            for (var el = 0; el < this.length; el++)
                func.call(this[el], el);
        }
        var defaults = {
                limit: 20,
                paginate: true,
				pager:{pageControl:function(){return this;}},
                makeSortable: true,
                tableClassName: 'simple_tabletop',
                divClass: 'simple_table'
            };
		/*this is the request for Ajax calls
		var defaultRequest={
			URL:"readDatat",
			PageSize: "the number of items per page"
			Page: "page index"
			SortOn: "col name"
			SortBy: "Asc or Desc"
		}*/
		/*var defaultResponse={
			Data:"DataToRead"
			Total: "num total items"P
			Page: "the page"
			PageSize: "the number of items per page"
		}*/
		//begin instanceof
		var tblObject = [];
		if(Data.url&&Data.pageSize)
		{
			var that =this;
			var request = Data;
			var ajaxPager={//pager;{
				pageControl:function(){
					var table =this.table;
					var elem = this.elem;
					table.append(simplify.paginate.call(this));
					J$(elem).append(table);
					return this;
				},
				sort:function(){
					this.clear();
					var that=this;
					
					$.ajax({
						url:request.url,
						data:{pageSize:request.pageSize,page:1,sortBy:sort.sortBy,sortOn:sort.sortOn},
						method:"Post",
						success:function(data){ 
							that.data=data.data.splice(1);
							ajaxPager.total=function(){
								return data.total;
							};
							ajaxPager.getN=function(){
								return 0;
							};
							ajaxPager.index=function(){
								return (data.pageSize *(data.page-1)+1);
							};
							ajaxPager.len=function(){
								return  Math.min((data.pageSize +(data.page-1)), data.total);
							};
							that.currentPage=1;
							that.makeTable().paginateFn();
						}
					});
				},
				updatePage:function(table,l){
					table.index = (l.index - 1) * table.limit;
					table.currentPage = l.index;
					table.clear();
					var that=table;
					
					$.ajax({
						url:request.url,
						data:{pageSize:request.pageSize,page:l.index,sortBy:sort.sortBy,sortOn:sort.sortOn},
						method:"Post",
						success:function(data){ 
							that.data=data.data.splice(1);
							ajaxPager.total=function(){
								return data.total;
							};
							ajaxPager.getN=function(){
								return 0;
							};
							ajaxPager.index=function(){
								return (data.pageSize *(data.page-1)+1);
							};
							ajaxPager.len=function(){
								return  Math.min((data.pageSize +(data.page-1)), data.total);
							};
							that.makeTable().paginateFn();
						}
					});
				}
			};
			defaults.limit = request.pageSize;
			defaults.pager=ajaxPager;
			
			$.ajax({
				url:request.url,
				data:{pageSize:request.pageSize,page:1,sortBy:"Asc",sortOn:"none"},
				method:"Post",
				success:function(data){
					Data = {data:data.data,limit:data.pageSize}; 
					ajaxPager.total=function(){
						return data.total;
					};
					ajaxPager.getN=function(){
						return 0;
					};
					ajaxPager.index=function(){
						return (data.pageSize *(data.page-1)+1);
					};
					ajaxPager.len=function(){
						return  Math.min((data.pageSize +(data.page-1)), data.data.length);
					};
					new table(defaults,that, Data);
				}
			});
			return this;
		} else {
			if (!(Data.data[0] instanceof Array))
			Data.data = simplify.BuildTableData(Data.data);
			this.each(
				function (index) {
					if (defaults.paginate)
					{
						defaults.pager= pager
					}
					var item = new table(defaults,this, Data);
					tblObject.push(item);
				}
			);
		}
        if (tblObject.length == 1)
            tblObject = tblObject[0];
        return tblObject;
    }
    /**
     * PLEASE NOTE, using the data uri schema as opposed to actually linking images results in the following
     * advantages:
     *  less overhead
     * disadvantages:
     *  IE6(+?) throws mixed content alerts when using https
     *  Does not cache
     */
    var Images =
    {
        arrows: function () {

            var data = "iVBORw0KGgoAAAANSUhEUgAAAA0AAAAYCAQAAACL+b/fAAAACXBIWXMAAAsSAAALEgHS3X78AAADGGlD";
            data += "Q1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjaY2BgnuDo4uTKJMDAUFBUUuQe5BgZERmlwH6egY2BmYGB";
            data += "gYGBITG5uMAxIMCHgYGBIS8/L5UBFTAyMHy7xsDIwMDAcFnX0cXJlYE0wJpcUFTCwMBwgIGBwSgltTiZ";
            data += "gYHhCwMDQ3p5SUEJAwNjDAMDg0hSdkEJAwNjAQMDg0h2SJAzAwNjCwMDE09JakUJAwMDg3N+QWVRZnpG";
            data += "iYKhpaWlgmNKflKqQnBlcUlqbrGCZ15yflFBflFiSWoKAwMD1A4GBgYGXpf8EgX3xMw8BSMDVQYqg4jI";
            data += "KAUICxE+CDEESC4tKoMHJQODAIMCgwGDA0MAQyJDPcMChqMMbxjFGV0YSxlXMN5jEmMKYprAdIFZmDmS";
            data += "eSHzGxZLlg6WW6x6rK2s99gs2aaxfWMPZ9/NocTRxfGFM5HzApcj1xZuTe4FPFI8U3mFeCfxCfNN45fh";
            data += "XyygI7BD0FXwilCq0A/hXhEVkb2i4aJfxCaJG4lfkaiQlJM8JpUvLS19QqZMVl32llyfvIv8H4WtioVK";
            data += "ekpvldeqFKiaqP5UO6jepRGqqaT5QeuA9iSdVF0rPUG9V/pHDBYY1hrFGNuayJsym740u2C+02KJ5QSr";
            data += "OutcmzjbQDtXe2sHY0cdJzVnJRcFV3k3BXdlD3VPXS8Tbxsfd99gvwT//ID6wIlBS4N3hVwMfRnOFCEX";
            data += "aRUVEV0RMzN2T9yDBLZE3aSw5IaUNak30zkyLDIzs+ZmX8xlz7PPryjYVPiuWLskq3RV2ZsK/cqSql01";
            data += "jLVedVPrHzbqNdU0n22VaytsP9op3VXUfbpXta+x/+5Em0mzJ/+dGj/t8AyNmf2zvs9JmHt6vvmCpYtE";
            data += "Frcu+bYsc/m9lSGrTq9xWbtvveWGbZtMNm/ZarJt+w6rnft3u+45uy9s/4ODOYd+Hmk/Jn58xUnrU+fO";
            data += "JJ/9dX7SRe1LR68kXv13fc5Nm1t379TfU75/4mHeY7En+59lvhB5efB1/lv5dxc+NH0y/fzq64Lv4T8F";
            data += "fp360/rP8f9/AA0ADzT6lvFdAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5Jf";
            data += "xUYAAADzSURBVHjanJKxagJBFEXPDEEICIKQKmD1BjvBRrCNX5CUFoKV37HVtnZ+gMRSP0D0FwQ7eakC";
            data += "aSIsBIQ0yqRw3R1lN4V3mrlzuDNvHg/PZUlF3qWSe0uuiD5R4LNMR47i5Sjdy4nxALhHNjQB2NHWXyC7";
            data += "ME4BNInPG+MB98KKUD1dg/G4GlsaV+iTlv5YYHwDoMEYjLwyp0hvRr55KkT7tPgiWe5BD670LcuoJDSy";
            data += "umBWAKa6MB5XZ8vzFfiipYkFTRjeZIaapBXqkkkAJrrMOg+uygYB4IO2HoJ/6YEBJ+DE4AyCAfBILF7i";
            data += "3FM+Uf+0928AftBmlQZccVYAAAAASUVORK5CYII=";
            return data;
        },
        up: function () {
            var data = "iVBORw0KGgoAAAANSUhEUgAAAA0AAAAYCAQAAACL+b/fAAAACXBIWXMAAAsSAAALEgHS3X78AAADGGlD";
            data += "Q1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjaY2BgnuDo4uTKJMDAUFBUUuQe5BgZERmlwH6egY2BmYGB";
            data += "gYGBITG5uMAxIMCHgYGBIS8/L5UBFTAyMHy7xsDIwMDAcFnX0cXJlYE0wJpcUFTCwMBwgIGBwSgltTiZ";
            data += "gYHhCwMDQ3p5SUEJAwNjDAMDg0hSdkEJAwNjAQMDg0h2SJAzAwNjCwMDE09JakUJAwMDg3N+QWVRZnpG";
            data += "iYKhpaWlgmNKflKqQnBlcUlqbrGCZ15yflFBflFiSWoKAwMD1A4GBgYGXpf8EgX3xMw8BSMDVQYqg4jI";
            data += "KAUICxE+CDEESC4tKoMHJQODAIMCgwGDA0MAQyJDPcMChqMMbxjFGV0YSxlXMN5jEmMKYprAdIFZmDmS";
            data += "eSHzGxZLlg6WW6x6rK2s99gs2aaxfWMPZ9/NocTRxfGFM5HzApcj1xZuTe4FPFI8U3mFeCfxCfNN45fh";
            data += "XyygI7BD0FXwilCq0A/hXhEVkb2i4aJfxCaJG4lfkaiQlJM8JpUvLS19QqZMVl32llyfvIv8H4WtioVK";
            data += "ekpvldeqFKiaqP5UO6jepRGqqaT5QeuA9iSdVF0rPUG9V/pHDBYY1hrFGNuayJsym740u2C+02KJ5QSr";
            data += "OutcmzjbQDtXe2sHY0cdJzVnJRcFV3k3BXdlD3VPXS8Tbxsfd99gvwT//ID6wIlBS4N3hVwMfRnOFCEX";
            data += "aRUVEV0RMzN2T9yDBLZE3aSw5IaUNak30zkyLDIzs+ZmX8xlz7PPryjYVPiuWLskq3RV2ZsK/cqSql01";
            data += "jLVedVPrHzbqNdU0n22VaytsP9op3VXUfbpXta+x/+5Em0mzJ/+dGj/t8AyNmf2zvs9JmHt6vvmCpYtE";
            data += "Frcu+bYsc/m9lSGrTq9xWbtvveWGbZtMNm/ZarJt+w6rnft3u+45uy9s/4ODOYd+Hmk/Jn58xUnrU+fO";
            data += "JJ/9dX7SRe1LR68kXv13fc5Nm1t379TfU75/4mHeY7En+59lvhB5efB1/lv5dxc+NH0y/fzq64Lv4T8F";
            data += "fp360/rP8f9/AA0ADzT6lvFdAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5Jf";
            data += "xUYAAACYSURBVHja7JAtDsJQDIC/PrEEhcJO9WWOBEOChROA5DZTs7sElhvAFUhw5LlJUCjMkiLYH+QF";
            data += "g+WrafulTVOMNjTRnSZ97ejJ2ZIP6m5mrrWa1rpoO2IA+BEnMgAuzMID6BYWjYCM4pWIAX7JgSGrcAQx";
            data += "/Jgz6ZuqmIa7A8oPASkliK7ZE2MjemUSVbfm+BiOv/pVfXnvcwBUti7d9mzFxwAAAABJRU5ErkJggg==";
            return data;
        },
        down: function () {
            var data = "iVBORw0KGgoAAAANSUhEUgAAAA0AAAAYCAQAAACL+b/fAAAACXBIWXMAAAsSAAALEgHS3X78AAADGGlD";
            data += "Q1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjaY2BgnuDo4uTKJMDAUFBUUuQe5BgZERmlwH6egY2BmYGB";
            data += "gYGBITG5uMAxIMCHgYGBIS8/L5UBFTAyMHy7xsDIwMDAcFnX0cXJlYE0wJpcUFTCwMBwgIGBwSgltTiZ";
            data += "gYHhCwMDQ3p5SUEJAwNjDAMDg0hSdkEJAwNjAQMDg0h2SJAzAwNjCwMDE09JakUJAwMDg3N+QWVRZnpG";
            data += "iYKhpaWlgmNKflKqQnBlcUlqbrGCZ15yflFBflFiSWoKAwMD1A4GBgYGXpf8EgX3xMw8BSMDVQYqg4jI";
            data += "KAUICxE+CDEESC4tKoMHJQODAIMCgwGDA0MAQyJDPcMChqMMbxjFGV0YSxlXMN5jEmMKYprAdIFZmDmS";
            data += "eSHzGxZLlg6WW6x6rK2s99gs2aaxfWMPZ9/NocTRxfGFM5HzApcj1xZuTe4FPFI8U3mFeCfxCfNN45fh";
            data += "XyygI7BD0FXwilCq0A/hXhEVkb2i4aJfxCaJG4lfkaiQlJM8JpUvLS19QqZMVl32llyfvIv8H4WtioVK";
            data += "ekpvldeqFKiaqP5UO6jepRGqqaT5QeuA9iSdVF0rPUG9V/pHDBYY1hrFGNuayJsym740u2C+02KJ5QSr";
            data += "OutcmzjbQDtXe2sHY0cdJzVnJRcFV3k3BXdlD3VPXS8Tbxsfd99gvwT//ID6wIlBS4N3hVwMfRnOFCEX";
            data += "aRUVEV0RMzN2T9yDBLZE3aSw5IaUNak30zkyLDIzs+ZmX8xlz7PPryjYVPiuWLskq3RV2ZsK/cqSql01";
            data += "jLVedVPrHzbqNdU0n22VaytsP9op3VXUfbpXta+x/+5Em0mzJ/+dGj/t8AyNmf2zvs9JmHt6vvmCpYtE";
            data += "Frcu+bYsc/m9lSGrTq9xWbtvveWGbZtMNm/ZarJt+w6rnft3u+45uy9s/4ODOYd+Hmk/Jn58xUnrU+fO";
            data += "JJ/9dX7SRe1LR68kXv13fc5Nm1t379TfU75/4mHeY7En+59lvhB5efB1/lv5dxc+NH0y/fzq64Lv4T8F";
            data += "fp360/rP8f9/AA0ADzT6lvFdAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5Jf";
            data += "xUYAAACVSURBVHja7NIxCsJAEIXhfxdJZSVY2c62ghcKpMpRcoQcIGibA+QQgu3Y2ihslTZsikiySray";
            data += "daYaPgZm4JlAqix/+pU27sl+VV6WMrFUWm25rECjrQm4HTcOH/DgqN6CeoqvnUL9+0LtqCOotQMwUwDc";
            data += "lisCwJ2T9tFf2pMzAAP5BECYWyoJUi1zTJmcJVtmkw7bOAAH0znlPznhTAAAAABJRU5ErkJggg==";
            return data;
        }
    }
})(jQuery);